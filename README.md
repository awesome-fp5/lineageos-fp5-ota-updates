# Note: currently on hold, because we experienced an EDL brick during development

# LineageOS FP5 OTA-Updates

Git repository containing recent INOFFICIAL builds of LineageOS 21 for the Fairphone 5 and OTA images.

You can find the builds in separate branches for each build date.
Older builds will be deleted automatically.

## Latest Builds (Sideload ZIP-Download)

- [21.0-20240226](https://www.technik-zeilen.de/blog/lineageos-21-fuer-das-fairphone-5/builds/lineage-21.0-20240226-UNOFFICIAL-FP5.zip) 
- [21.0-20240225](https://gitlab.com/awesome-fp5/lineageos-fp5-ota-updates/-/raw/21.0-20240225/lineage-21.0-20240225-UNOFFICIAL-FP5.zip)
- [21.0-20240217](https://gitlab.com/awesome-fp5/lineageos-fp5-ota-updates/-/raw/21.0-20240217/lineage-21.0-20240217-UNOFFICIAL-FP5.zip?ref_type=heads)
